<?php
session_start();
ob_start();
?>    
<!DOCTYPE html>
<html lang="en">
<head>
<title>RSM GP Access</title>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="bootstrap/css/bootstrap.css">
<script src="bootstrap/js/bootstrap.min.js"></script>
<script src="https://code.jquery.com/jquery-3.4.1.slim.min.js"></script>
</head>
<body>
	<div id=ourServicesJumbo class="jumbotron-fluid">
		<h1 class="display-4">RSM HVAC System</h1>
	</div>

	<div class="container">
            <div class="row justify-content-center">
                <div class="col-md-6">
                    <h3>Current readings</h3>
                    <form action="" method="post" name="signin-form">
                        <div class="form-group">
                            <input type="text" name ="Current temperature" class="form-control" placeholder="Your Current temperature is - " required />
                        </div>
                        <div class="form-group">
                            <input type="text" name ="proximity" class="form-control" placeholder="Motion is -" required />
                        </div>
                        <div class="form-group">
                            <input type="text" name ="Set temperature" class="form-control" placeholder="Please set your desired temperature" required />
                            	<select name="temperature" id="temperature">
  									<option value="10">10</option>
  									<option value="11">11</option>
  									<option value="12">12</option>
  									<option value="13">13</option>
  									<option value="14">14</option>
  									<option value="15">15</option>
  									<option value="16">16</option>
  									<option value="17">17</option>
  									<option value="18">18</option>
  									<option value="19">19</option>
  									<option value="20">20</option>
  									<option value="21">21</option>
  									<option value="22">22</option>
  									<option value="23">23</option>
  									<option value="24">24</option>
  									<option value="25">25</option>
  									<option value="26">26</option>
  									<option value="27">27</option>
  									<option value="28">28</option>
								</select>
                        </div>
                        <div class="form-group">
                            <input type="submit" class="btnSubmit" name ="login" value="Login">
                        </div>
                        <div class="form-group">
                            <a >Please Contact customer services if you are having issues</a>
                            <a ></a>
                            <a >Has one of our Engineers contacted you about extended warranty? Get in touch today!!!</a>
                        </div>
                    </form>
                    <?php
//                     include('Includes/config.php');
//                         if (isset($_POST['login'])) {
//                             $email = $_POST['email'];
//                             $password = $_POST['password'];
//                             $query = $connection->prepare("SELECT * FROM Admin WHERE Email=:email");
//                             $query->bindParam("email", $email, PDO::PARAM_STR);
//                             $query->execute();
//                             $result = $query->fetch(PDO::FETCH_ASSOC);
//                             if (!$result) {
//                                 echo '<span style="color:red;text-align:center;">Email password combination is wrong!</span>';
//                             } else {
//                                 if ($password === $result['Password']) {
//                                     $_SESSION['user_id'] = $result['id'];
//                                     header("location: account.php");
//                                 } else {
//                                     echo '<span style="color:red;text-align:center;">Email password combination is wrong!</span>';
//                                 }
//                             }
//                         }
//                     ?>
                </div>
            </div>
        </div>
</body>
</html>
